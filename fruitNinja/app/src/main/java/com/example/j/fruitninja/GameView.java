package com.example.j.fruitninja;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by J on 2/28/16.
 */


public class GameView extends View {

    private Paint mBorderPaint = new Paint();
    private View self;
    private Timer mTimer;

    private int StartY;
    private int leftStartX;
    private int middleLeftStartX;
    private int middleRightStartX;
    private int rightStartX;

    private int timerSpeed = 75;

    //dimensions of screen
    private int width;
    private int height;

    //for where the object spawns
    private int randomStart;
    private int randomPath;

    private int gameOver = 0;
    private int objectClicked = 0;
    int score = 0;
    private int startNew = 0;
    //
    int positionX, positionY;     // Position of the character
    float velocityX, velocityY;     // Velocity of the character
    float gravity = 0.5f;           // How strong is gravity

    Rect r;

    int randomColor;

    //signifies that a new object was just made
    int newRun = 1;
    
    public GameView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        width = metrics.widthPixels;
        height = metrics.heightPixels;


        StartY = height - 50 ;
        leftStartX = width / 5;
        middleLeftStartX = 3 * (width / 5);
        middleRightStartX = 3 * (width / 5);
        rightStartX = 4 * (width / 5);

        self = this;

        mTimer = new Timer();
        Log.i("MyApp", String.valueOf(width) + " "  + String.valueOf(height));
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(startNew == 1){
                    startNew = 0;
                    gameOver = 0;
                    newRun = 1;
                    objectClicked = 0;
                }

                else{
                    if(objectClicked == 1){

/*
                        TextView myScoreView = (TextView) findViewById(R.id.scoreValue);

                        if(myScoreView == null){
                            Log.i("MyValue", "why am i null");
                        }
                        int scoreNow =Integer.parseInt(myScoreView.getText().toString()) + 1;

                       // myScoreView.setText(""+scoreNow);*/
                        newRun = 1;
                        objectClicked = 0;
                    }
                    else {
                        if (gameOver == 0) {

                            if (newRun == 1) {
                                //random color for object
                                Random randColor = new Random();
                                randomColor = randColor.nextInt(7);
                                switch (randomColor) {
                                    case 0:
                                        mBorderPaint.setColor(Color.RED);
                                        break;
                                    case 1:
                                        mBorderPaint.setColor(Color.BLUE);
                                        break;
                                    case 2:
                                        mBorderPaint.setColor(Color.YELLOW);
                                        break;
                                    case 3:
                                        mBorderPaint.setColor(Color.CYAN);
                                        break;
                                    case 4:
                                        mBorderPaint.setColor(Color.GREEN);
                                        break;
                                    case 5:
                                        mBorderPaint.setColor(Color.WHITE);
                                        break;
                                    case 6:
                                        mBorderPaint.setColor(Color.MAGENTA);
                                        break;
                                    default:
                                        break;
                                }

                                // r = new Rect(positionX, positionY, positionX + 100, positionY + 100);
                                newRun = 0;
                                //Randomizes where shape starts from
                                Random randStart = new Random();
                                randomStart = randStart.nextInt(4);

                                positionY = StartY;
                                switch (randomStart) {
                                    //start from left
                                    case 0:
                                        positionX = leftStartX;
                                        break;
                                    //start from middleLeft
                                    case 1:
                                        positionX = middleLeftStartX;
                                        break;
                                    //start from middleRight
                                    case 2:
                                        positionX = middleRightStartX;
                                        break;
                                    //start from right
                                    case 3:
                                        positionX = rightStartX;
                                        break;
                                    default:
                                        break;
                                }
                                Random randDirection = new Random();
                                randomPath = randStart.nextInt(3);
                                velocityY = -24.0f;

                            }
                            r = new Rect(positionX, positionY, positionX + 100, positionY + 100);
                            self.postInvalidate();
                            switch (randomPath) {
                                case 0:
                                    pathToLeft(randomStart);
                                    break;
                                case 1:
                                    pathOnlyUp();
                                    break;
                                case 2:
                                    pathToRight(randomStart);
                                    break;
                                default:
                                    break;
                            }
                            Log.i("MyValues", String.valueOf(positionX) + " " + String.valueOf(positionY));
                        }
                        if (positionY + 20 > height) {
                            gameOver = 1;
                            Log.i("MyValues", "Game Over");
                        }
                    }
                }
            }
        }, 0, timerSpeed);
    }

    //object only goes in upwards motion
    public void pathOnlyUp(){
        velocityX = 0.0f;
        velocityY += gravity * .81;
        positionX += velocityX * .81;
        positionY += velocityY * .81;
    }

    //need to know where it started to determine its velocity in left direction
    //if started in left position we only go slightly to left
    //if middle or right positions we go more to the left
    //we will use its randomStartLocation for this
    public void pathToLeft(int typeOfStart){
        switch(typeOfStart){
            case 0:
                velocityX = - 1.0f;
                break;
            case 1:
                velocityX = -2.5f;
                break;
            case 2:
                velocityX = -4.0f;
                break;
            case 3:
                velocityX = -6.0f;
                break;
            default:
                break;
        }
        velocityY += gravity * .81;
        positionX += velocityX * .81;
        positionY += velocityY * .81;
    }

    //need to know where it started to determine its velocity in right direction
    //similar concept to the left direction
    public void pathToRight (int typeOfStart){
        switch(typeOfStart){
            case 0:
                velocityX = 6.0f;
                break;
            case 1:
                velocityX = 4.0f;
                break;
            case 2:
                velocityX = 2.5f;
                break;
            case 3:
                velocityX = 1.0f;
                break;
            default:
                break;
        }
        velocityY += gravity * .81;        // Apply gravity to vertical velocity
        positionX += velocityX * .81;      // Apply horizontal velocity to X position
        positionY += velocityY * .81;      // Apply vertical velocity to X position
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction() & MotionEvent.ACTION_MASK;
        if (action == MotionEvent.ACTION_DOWN) {
            int index = event.getActionIndex();
            int lastX = (int)event.getX(index);
            int lastY = (int)event.getY(index);
            if(r.intersects(lastX,lastY,lastX+1,lastY+1)){
                Log.i("MyValue","intersection here");
                r.setEmpty();
                objectClicked = 1;
            }
        }
        return true;
    }

    @Override
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        canvas.drawRect(r, mBorderPaint);

    }

    public void startNewGame(){
        startNew = 1;
    }

}
